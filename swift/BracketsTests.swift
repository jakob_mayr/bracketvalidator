//
//  BracketsTests.swift
//  Dojo
//
//  Created by Cordt Voigt on 24.07.17.
//  Copyright © 2017 Cordt Voigt. All rights reserved.
//

import XCTest
@testable import Dojo

class BracketsTests: XCTestCase {
    
    
    func testBalancedBracketsShouldPass() {
        let testCases = [
            "(abc)",
            "{abc}",
            "[abc]",
            "(a[b{c}b]a)",
            "(())",
            "{a(())b}"
        ]
        
        testCases.forEach {
            XCTAssertTrue(valid($0), "should detect valid brackets for \($0)")
        }
    }
    
    
    func testUnbalancedBracketsShouldFail() {
        let testCases = [
            "(abc",
            "{abc",
            "[abc",
            "(a[bc}b]a)",
            "(()",
            "{a())b}"
        ]
        
        testCases.forEach {
            XCTAssertFalse(valid($0), "should detect invalid brackets for \($0)")
        }
    }
    
    
    func testNoBracketsShouldPass() {
        let testCases = [
            "abc",
            ""
        ]
        
        testCases.forEach {
            XCTAssertTrue(valid($0), "should detect valid brackets for \($0)")
        }
    }
    
}
