//
//  Brackets.swift
//  Dojo
//
//  Created by Cordt Voigt on 24.07.17.
//  Copyright © 2017 Cordt Voigt. All rights reserved.
//

import Foundation


enum Bracket: Character {
    case openRound = "(", openCurly = "{", openSquared = "["
    case closeRound = ")", closeCurly = "}", closeSquared = "]"
    
    static var opener: [Bracket] {
        return [.openRound, .openCurly, .openSquared]
    }
    
    static var closer: [Bracket] {
        return [.closeRound, .closeCurly, .closeSquared]
    }
    
    static var all: [Bracket] {
        return opener + closer
    }
    
    func matches(_ character: Character) -> Bool {
        return self.rawValue == character
    }
    
    func closes(_ opener: Bracket) -> Bool {
        switch self {
        case .closeRound:
            return opener == .openRound
            
        case .closeCurly:
            return opener == .openCurly
            
        case .closeSquared:
            return opener == .openSquared
            
        default:
            return false
        }
    }
}


func matchingBracket(for character: Character) -> Bracket? {
    return Bracket.all.reduce(nil) {
        let match = $0.1.matches(character)
        return match ? $0.1 : $0.0
    }
}


func valid(_ string: String) -> Bool {
    let chars = string.characters
    var openers = [Bracket]()

    return chars.enumerated().reduce(true) {
        guard $0.0 else { return false }
        
        let match = matchingBracket(for: $0.1.element)
        if let match = match {
            if Bracket.opener.contains(match) {
                openers.append(match)
                
            } else if Bracket.closer.contains(match) {
                guard !openers.isEmpty else { return false }
                guard match.closes(openers.removeLast()) else { return false }
            }
        }
        
        if $0.1.offset == chars.count - 1 {
            guard openers.isEmpty else { return false }
        }
        return true
    }
}
