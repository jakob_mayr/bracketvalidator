import java.util.Date
import scala.io.Source
import org.scalatest._

class BracketSpec extends FlatSpec with Matchers {

  // test data
  val realCode = Source.fromFile("/Users/jakobmayr/everskill/BracketValidator/src/BracketValidator.scala").mkString // @TODO hardcoded address, please change
  val testCases = Map(
            "{ [ Boring code ] ( More boring code ) }" ->true,
            "{ [ Some boring code ]" -> false,
            "{ [ ( ] ) }" -> false,
            "{ [ Boring code with operators + - * / }" -> false,
            "{ [ Correct code with operators + - * / ] ( More code with operators + - * / ) }" -> true)


  // Here are the tests
  "A StackValidator" should "validate all kinds of code" in { takeTime(test, new StackValidator()) } // with time tracking

  "And a RegexValidator" should "validate all kinds of code" in { test(new RegexValidator(), testCases + (realCode -> true)) } // and without timetracking, but with real code



  def test(validator: BracketValidator, cases: Map[String, Boolean]) = cases foreach { case (testString, expected) => validator validateCode testString should be (expected) }

  def takeTime(testFunction: (BracketValidator, Map[String, Boolean])  => Unit, validator: BracketValidator, cases: Map[String, Boolean] = testCases) = {
    val start = new Date()

    (1 to 10000).foreach { _ => testFunction(validator, cases) }

    val end = new Date()
    println(end.getTime - start.getTime)
  }

}