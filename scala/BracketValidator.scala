import scala.collection.mutable

trait BracketValidator {
  def validateCode(code: String) : Boolean
}


class StackValidator extends BracketValidator {
  private val brackets = new mutable.Stack[Char]

  def validateCode(code: String): Boolean = { // Not threadsafe
    brackets.clear() // Cleanup First to allow multiple tests
    val validated = code.toList.map { validateNextChar(_) } // Check every char
    val filtered = validated.filter(p => !p) // Just keep the errors
    filtered.isEmpty && brackets.isEmpty // No errors and no open brackets
  }

  private def validateNextChar(nextChar: Char): Boolean =
    nextChar match {
      case '[' | '{' | '(' => {
        brackets.push(nextChar)
        true
      }
      case ']' => brackets.pop().equals('[')
      case '}' => brackets.pop().equals('{')
      case ')' => brackets.pop().equals('(')
      case _ => true
    }
}

class RegexValidator extends BracketValidator {

  def validateCode(code: String) : Boolean = {
    val strippedCode = code
      .replaceAll("\\s", "") // whitespace
      .replaceAll("\\w", "") // text and digits
      .replaceAll("\".*\"", "").replaceAll("'.'", "") // String literals
      .replaceAll("[\\p{Punct}&&[^\\(\\)\\[\\]\\{\\}]]", "") // punctuation except brackets
    val validated = removePairedBrackets(strippedCode) // remove closed brackets
    validated.isEmpty // No more open brackets or other chars
  }

  def removePairedBrackets(code: String) : String = {
    val strippedCode = code.replaceAll("\\(\\)", "").replaceAll("\\[\\]", "").replaceAll("\\{\\}", "") // remove closed brackets
    if (code.equals(strippedCode)) {
      code
    } else {
      removePairedBrackets(strippedCode)
    }
  }
}
