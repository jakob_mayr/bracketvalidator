//: Playground - noun: a place where people can play

import UIKit
import XCTest

func bracesValidator(code: String) -> Bool {
    var bracesStack: [Character] = []
    for singleCharacter in code.characters {
        switch singleCharacter {
        case "(":
            bracesStack.append(singleCharacter)
        case "[":
            bracesStack.append(singleCharacter)
        case "{":
            bracesStack.append(singleCharacter)
        case ")":
            if bracesStack.last == "(" { bracesStack.popLast() } else { return false }
        case "]":
            if bracesStack.last == "[" { bracesStack.popLast() } else { return false }
        case "}":
            if bracesStack.last == "{" { bracesStack.popLast() } else { return false }
        default:
            break
        }
    }
    return true
}

func bracesValidatorWithDict (code: String) -> Bool {
    enum Either {
        case tokens([Character])
        case fail
    }
    let braces: [Character : Character] = ["(": ")","[":"]","{":"}"]
    let tokens = code.characters.reduce(Either.tokens([])) { acc, val in
        guard case .tokens(let chars) = acc else { return .fail }
        switch val {
        case "(", "[", "{":
            return .tokens(chars + [val])
        case "}", ")", "]":
            guard let last = chars.last, val == braces[last] else { return .fail }
            return .tokens(Array(chars.dropLast()))
        default:
            return .tokens(chars)
        }
    }
    guard case .tokens(let result) = tokens, result.count == 0 else { return false }
    return true
    
}
let str1 = "{ [ ] ( ) }"
let str2 = "{ [ ( ] ) }"
let str3 = "{ [ }"
let str4 = "{ sss[as ]ddd (asdv == )ddd }"
bracesValidatorWithDict(code: str1)
bracesValidatorWithDict(code: str2)
bracesValidatorWithDict(code: str3)
bracesValidatorWithDict(code: str4)
bracesValidator(code: str1)
bracesValidator(code: str2)
bracesValidator(code: str3)
bracesValidator(code: str4)

class myTests: XCTestCase {
    let str1 = "{ [ ] ( ) }"
    let str2 = "{ [ ( ] ) }"
    let str3 = "{ [ }"
    let str4 = "{ sss[as ]ddd (asdv == )ddd }"
    func testValidator() {
        XCTAssertTrue(bracesValidator(code: str1))
        XCTAssertFalse(bracesValidator(code: str2))
        XCTAssertFalse(bracesValidator(code: str3))
        XCTAssertTrue(bracesValidator(code: str4))
    }
    func testValidatorWithDict() {
        XCTAssertTrue(bracesValidatorWithDict(code: str1))
        XCTAssertFalse(bracesValidatorWithDict(code: str2))
        XCTAssertFalse(bracesValidatorWithDict(code: str3))
        XCTAssertTrue(bracesValidatorWithDict(code: str4))
    }
}



