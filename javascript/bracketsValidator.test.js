const inputArray 		= ["{ [ ] ( ) }" , "{ [ ( ] ) }" , "{[}" ];
const outputExpected 	= ["YES", "NO", "NO"];
describe('[bracketsValidator.js] ', () => {
    it('testing isBalanced() ', () => {
        expect(isBalanced(inputArray[0])).toBe(true);
        expect(isBalanced(inputArray[1])).toBe(false);
        expect(isBalanced(inputArray[2])).toBe(false);
    })
    it('testing braces() ', () => {
		let outputArray = braces(inputArray);
		for (var i in outputArray) {
			expect(outputArray[i])===outputExpected[i]).toBe(true);
		}
    })
});