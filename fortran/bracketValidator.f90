program test
    !
    implicit none

    logical :: x
    integer :: position, count

    position = 0
    count = 0

    write (*,*), "Empty Input (T):", validate("")
    write (*,*), "asdf (T):", validate("asdf")
    write (*,*), "{[]()} (T):", validate("{[]()}")
    write (*,*), "[}} (F):", validate("[}}")
    write (*,*), "]}} (F):", validate("]}}")
contains

    logical function validate( input )
        !
        implicit none
        character(len = *), intent(in) :: input
        character(len = 1), dimension(len_trim(input)) :: stack
        integer :: position, n, i
        logical :: result

        n = len_trim(input)
        result = .FALSE.
        position = 1
        do i = 1, n
            if (findOpenerIndex(input(i : i)) > 0) then
                stack(position) = input(i : i)
                position = position + 1
            else if (findCloserIndex(input(i : i)) > 0) then
                if(findOpenerIndex(stack(position - 1)) == findCloserIndex(input(i : i))) then
                    position = position - 1
                else
                    position = -1
                    exit
                end if
            else
                continue
            end if
        end do
        result = (position == 1)
        validate = result
    end function validate

    integer function findOpenerIndex( input )
        !
        implicit none
        character(len = 1), intent(in) :: input
        integer :: result
        integer :: i
        character(len = 1), dimension(3) :: openers
        openers = ["{", "[", "("]

        result = 0
        do i = 1, 3
            if (openers(i) .eq. input) then
                result = i
                exit
            endif
        end do
        findOpenerIndex = result
    end function findOpenerIndex

    integer function findCloserIndex( input )
        !
        implicit none
        character(len = 1), intent(in) :: input
        integer :: result
        integer :: i
        character(len = 1), dimension(3) :: closers
        closers = ["}", "]", ")"]

        result = 0
        do i = 1, 3
            if (closers(i) .eq. input) then
                result = i
                exit
            endif
        end do
        findCloserIndex = result
    end function findCloserIndex
end program test