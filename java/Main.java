package bracketsproblem;

import java.util.Scanner;

/**
 * Created by marcoscandelaboti on 21/7/17.
 */
public class Main {

    public static void main(String[] args) {
        BracketsUtils bracketsUtils = new BracketsUtils();

        Scanner reader = new Scanner(System.in);  // Reading from System.in
        System.out.println("Enter your text: ");
        String text = reader.nextLine();
        System.out.println(bracketsUtils.verify(text));
    }
}