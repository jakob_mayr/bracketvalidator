package bracketsproblem;

import java.util.Stack;

/**
 * Created by marcoscandelaboti on 21/7/17.
 */
public class BracketsUtils {


    private char getPair(char x) {
        if (x == ')')
            return '(';
        if (x == ']')
            return '[';
        if (x == '}')
            return '{';
        throw new IllegalArgumentException();
    }


    boolean verify(String str) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < str.length(); i++) {

            if (str.charAt(i) == '{' | str.charAt(i) == '[' | str.charAt(i) == '(') {
                stack.push(str.charAt(i));
            } else if (str.charAt(i) == '}' | str.charAt(i) == ']' | str.charAt(i) == ')') {
                if (stack.size() == 0) {
                    return false;
                }
                char previousBracket = stack.pop();
                if (previousBracket != getPair(str.charAt(i))) {
                    return false;
                }
            }
        }
        return stack.size() == 0;
    }

}
