package bracketsproblem;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by marcoscandelaboti on 21/7/17.
 */
public class BracketsTest {

    @Test
    public void testBrackets(){
        BracketsUtils bracketsUtils = new BracketsUtils();

        assertTrue(bracketsUtils.verify("{ [ ] ( ) }"));
        assertFalse(bracketsUtils.verify("{ [ ( ] ) }"));
        assertFalse(bracketsUtils.verify("{ [ }"));
    }

}
